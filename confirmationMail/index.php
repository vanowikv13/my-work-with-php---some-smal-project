<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration</title>
</head>

<body>
    <h2>Registration</h2>

    <form action="register.php" method="post">
        username: <input type="text" name="name">
        password: <input type="password" name='password'>
        email: <input type="email" name="email">
        <input type="submit" value="submit">
    </form>
        <?php
            if(isset($_SESSION['message'])) {
                echo '<script type="text/javascript">alert("'.$_SESSION['message'].'");</script>';
                unset($_SESSION['message']);
            }

            if(isset($_SESSION['exception'])) {
                echo '<script type="text/javascript">alert("'.$_SESSION['exception'].'");</script>';
                unset($_SESSION['exception']);
            }
        ?>
</body>

</html>