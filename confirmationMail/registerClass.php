<?php

session_start();
//for database connection
require_once 'conClass.php';

//for mail with confirmation
require_once 'send_mail_class.php';

class Registration {
    protected $conn;
    private $pass;
    protected $username;
    protected $email;
    protected $userid;
    protected $key;
    protected $sendmail;

    public function __construct() {
        $this->conn = new DatabaseConnection();
        $this->sendmail = new MailingService();
    }

    protected function getDataFromPOST() {
        if(!isset($_POST['password']) || strlen($_POST['password']) < 6 || !isset($_POST['name']) || strlen($_POST['name']) < 2 || !isset($_POST['email']) || strlen($_POST['email']) < 4)
            throw new Exception("Error: The data you sent is incorrect. Please try again.");

            $this->pass = md5($_POST['password']);
            $this->username = $_POST['name'];
            $this->email = $_POST['email'];
    }

    //return ID of a user
    protected function insertUserData() {
        $insertUserSql = "insert into users(username, password, email, active) VALUES('{$this->username}', '{$this->pass}', '{$this->email}', false);";
        if(!$this->conn->query($insertUserSql))
            throw new Exception("Error: The data you sent was not added to the database. Try again or contact our technical department.");

        //get user id and insert it
        $this->userid = $this->conn->insertID();
        return $this->userid;
    }

    protected function createAndReturnKey() {
        $this->key = md5($this->username . $this->email . date('dmYHis') . $this->userid);
        return $this->key;
    }

    protected function insertConfirmation() {
        if(!$this->conn->query("insert into confirm(userid, `key`, email) value('{$this->userid}', '{$this->key}', '{$this->email}')"))
            throw new Exception("Error: Problem with inserting data about confirmation please try again in few minutes.");
    }

    protected function sendConfirmationMail() {
        //adres to confirm
        $address = "http://localhost/confirmationMail/user-confirmation.php?email={$this->email}&code={$this->key}";

        //send confirmation mail to the user
        $this->sendmail->sendMail($this->email, $this->username, "Click: {$address}", 'Mail confirmation');
    }

    //for user that want to send e-mail confirmation again
    public function confirmationAgain($username, $email, $userid) {
        //update user data
        $this->username = $username;
        $this->email = $email;
        $this->userid = $userid;

        try {
            $this->confirmation();
        } catch(Exception $e) {
            $_SESSION['exception'] = $e->getMessage();
            //header("Location: index.php");
        }

        //header("LOcation: waitingRoom.php");
    }

    protected function confirmation() {
            //create key with confirmation and send it
            $this->createAndReturnKey();

            //insert confirmation key to DB
            $this->insertConfirmation();

            //send confirmation key again
            $this->sendConfirmationMail();
    }

    public function registration() {
        try {
            //get data from registration form
            $this->getDataFromPOST();
            //insert user data to DB
            $this->insertUserData();

            //all confirmation stuff
            $this->confirmation();

        } catch(Exception $e) {
            $_SESSION['exception'] = $e->getMessage();
        } catch (Message $m) {
            $_SESSION['message'] = $m->getMessage();
        }

        //then direct us to other location if there is any problem it's direct us to index
        if(isset($_SESSION['exception'])) {
            header("Location: index.php");
        }
        else {
            header("Location: waitingRoom.php");
            $_SESSION['username'] = $this->username;
            $_SESSION['email'] = $this->email;
            $_SESSION['userid'] = $this->userid;
        }
    }
}

?>