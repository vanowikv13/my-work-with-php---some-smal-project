<?php

require "vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


/*
    Class give possibility to send mail to the user
*/

class MailingService {
    public $fromEmailAddress = 'blogvanowikv13@gmail.com';
    public $name = "Mariusz Nowicki";
    private $pass = 'obuazarUfEOKV9wUdPxX';
    protected $mail;

    public function __construct() {
        //use PHPMailer class
        $this->mail = new PHPMailer(true);
        //config all the data
        $this->config();
    }

    public function config() {
        $this->mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $this->mail->isSMTP();                                      // Set mailer to use SMTP
        $this->mail->Host = 'smtp.gmail.com';                       //main smtp server
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = $this->fromEmailAddress;         // SMTP username
        $this->mail->Password = $this->pass;             //SMTP password
        $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = 587;
        $this->mail->CharSet = 'UTF-8';
    }

    public function sendMail($to, $name, $body, $subject) {
        try {
            $this->mail->setFrom($this->fromEmailAddress, $this->name);
            $this->mail->addAddress($to, $name);

            //Content
            $this->mail->Subject = $subject;
            $this->mail->Body = $name." with ".$to.", register on ".date("d.m.Y").", at ".date("H:i:s")."<br>".$body;

            $this->mail->send();
        } catch(Exception $e) {
            //this exception goes to register.php catch block
            throw new Exception("ERROR: the mail was not sent. Mailer Error:".$this->mail->ErrorInfo);
        }
    }

}

?>