var form = document.getElementsByTagName('form')[0];
var email = document.getElementsByName('mail')[0];
var NIP = document.getElementsByName("NIP")[0];

var error, resolve = false;

var resolve = function () {
    if (resolve) {
        error.innerHTML = ""; // Reset the content of the message
        error.className = "emailError"; // Reset the visual state of the message
    }
}

var classOP = function(stat, elem) {
    elem.classList.add("inputI")
}

email.addEventListener("input", function (event) {
    if (email.validity.valid && email.value.length != 0) {
        error = document.querySelector('.emailError');
        error.innerHTML = "";
        error.className = "emailError";
    }
}, false);

NIP.addEventListener("input", function (event) {
    console.log(/^\d+$/.test(NIP.value));
    if (NIP.value.length == 10 && /^\d+$/.test(NIP.value)) {
        //error = document.querySelector('.NIPError');
        NIP.classList.remove("inputInvalid");
    } else {
        NIP.classList.add("inputInvalid");
    }
}, false);

form.addEventListener("submit", function (event) {
    let prevent = false;
    let error;
    if (!email.validity.valid || email.value.length == 0) {
        error = document.querySelector('.emailError');
        error.innerHTML = "I expect email adres, for example: name@domain.com<br>";
        error.className = "error active";
        prevent = true;
    }
    if (NIP.value.length != 10 || !/^\d+$/.test(NIP)) {
        error = document.querySelector('.NIPError');
        error.innerHTML = "NIP contain only number, size of NIP should be also 10<br>";
        error.className = "error active";
        prevent = true;
    }

    if (prevent)
        event.preventDefault();
}, false);